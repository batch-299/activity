<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    // connection the Post.php model
// establishes that a single user can have many posts
    // we can call $user->posts to get all of the posts of a single user later on.
    public function posts(){
        return $this->hasMany('App\Models\Post');
    }

    // get all likes of a specific user
    public function likes(){
        return $this->hasMany('App\Models\PostLike');
    }

    public function comments(){
        return $this->hasMany('App\Models\PostComment');
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];
}

// blade syntax
// {{}}
// <h1>{{$post->title}}</h1>

// condition statement
// @if
// @else
// @endif

// sample
// @if($post !== null)
//  <h1>{{$post->title}}</h1>
// @endif

